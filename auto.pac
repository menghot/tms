function FindProxyForURL(url, host) {
  if (isPlainHostName(host) || dnsDomainIs(host, ".chatgpt.com")) {
    return "PROXY 10.100.1.1:8118; DIRECT";
  } else {
    return "DIRECT";
  }
}
