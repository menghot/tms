#!/bin/sh


source /etc/profile

cd /root/tms

update_ip() {
  ip=$(curl http://42.194.220.15:8090/headers)
  if [ -n "$ip" ]; then
    echo $ip > ip
    git pull
    git add .
    git commit -m 'update'
    git push -f
    echo "new ip", $ip
  fi
}


#if [ ! "$(curl https://api.ipify.org)" = "$(curl https://gitee.com/menghot/tms/raw/master/ip)" ]; then update_ip; fi
if [ ! "$(curl http://42.194.220.15:8090/headers)" = "$(cat ip)" ]; then update_ip; fi


